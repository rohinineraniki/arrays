let output_array = [];
function map(items, callBackFunction) {
  if (Array.isArray(items)) {
    for (let index = 0; index < items.length; index++) {
      const array_value = callBackFunction(items[index], index, items);
      output_array.push(array_value);
    }
    return output_array;
  } else {
    return [];
  }
}
module.exports = map;

// let result = undefined;
// function eachValue(value, index, items) {
//   result = parseInt(value) ** 2;
//   if (result !== undefined) {
//     return result;
//   }
//   return result;
// }

// module.exports = eachValue;
