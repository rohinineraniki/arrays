let output_array = [];
function filter(items, callBackFunction) {
  if (Array.isArray(items)) {
    for (let index = 0; index < items.length; index++) {
      const array_value = callBackFunction(items[index], index, items);
      if (array_value === true) {
        output_array.push(items[index]);
      }
    }
    return output_array;
  } else {
    return [];
  }
}
module.exports = filter;
