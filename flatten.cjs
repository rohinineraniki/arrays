function flatten(input, depth, output) {
  output = output || [];

  if (!depth && depth !== 0) {
    depth = 1;
  } else if (depth <= 0) {
    return output.concat(input);
  }
  let idx = output.length;
  for (let index = 0, length = input.length; index < length; index++) {
    let value = input[index];
    if (Array.isArray(value) && value !== undefined) {
      if (depth > 1) {
        flatten(value, depth - 1, output);
        idx = output.length;
      } else {
        let x = 0,
          len = value.length;

        while (x < len) {
          if (value[x] !== undefined) {
            output[idx++] = value[x];
          }
          x++;
        }
      }
    } else if (value !== undefined) {
      output[idx++] = value;
    }
  }
  return output;
}

module.exports = flatten;

// function flatten(inputArray) {
//   let outputArray = [];
//   recursion(0, inputArray, outputArray);
//   return outputArray;
// }
// function recursion(index, inputArray, outputArray) {
//   if (index >= inputArray.length) return;

//   if (Array.isArray(inputArray[index])) {
//     recursion(0, inputArray[index], outputArray);
//     console.log(index);
//   } else {
//     outputArray.push(inputArray[index]);
//   }
//   recursion(index + 1, inputArray, outputArray);
// }
// flatArray = flatten([1, 2, [3, 4, [5, 6], 7, [8, [9, 91]], 10], 11, 12, []]);
// console.log(flatArray);

// const flatten_array = [];
// let count = 0;
// function flatten(items, d) {
//   for (let index = 0; index < items.length; index++) {
//     if (Array.isArray(items[index])) {
//       count += 1;
//       if (count === d) {
//         flatten_array.push(items[index]);
//         flatten(items[index], d);
//         count = 0;
//       }
//       flatten(items[index], d);
//     } else {
//       flatten_array.push(items[index]);
//       console.log(items[index]);
//     }
//   }
//   return flatten_array;
// }

// const sampleArray = [1, 2, [3, 4, [5, 6], 7, [8, [9, 91]], 10], 11, 12, []];
