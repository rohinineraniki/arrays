function reduce(items, callBackFunction, accumulator) {
  if (accumulator !== undefined) {
    for (let index = 0; index < items.length; index++) {
      const result = callBackFunction(accumulator, items[index], index, items);
      accumulator = result;
    }
    return accumulator;
  } else {
    accumulator = items[0];
    for (let index = 1; index < items.length; index++) {
      const result = callBackFunction(accumulator, items[index], index, items);
      accumulator = result;
    }
    return accumulator;
  }
}
module.exports = reduce;
