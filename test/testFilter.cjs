const filter = require("../filter.cjs");
const words = [
  "spray",
  "limit",
  "elite",
  "exuberant",
  "destruction",
  "present",
];

function callBackFunction(value, index, items) {
  return value.length > 6 === true;
}

const result = filter(words, callBackFunction);
console.log(result);

// const eachValue = require("../filter.cjs");

// const items = [1, 2, 3, 4, 5, 5];
// let filterd_array = [];
// function filter(items) {
//   for (let index = 0; index < items.length; index++) {
//     let filterd_value = eachValue(items[index], index, items);
//     if (filterd_value !== undefined) {
//       filterd_array.push(filterd_value);
//     }
//   }
//   return filterd_array;
// }

// const output = filter(items);
// console.log(output);
