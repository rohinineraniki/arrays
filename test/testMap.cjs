const map = require("../map.cjs");
const items = [1, 2, 3, 4, 5, 5];

function callBackFunction(value, index, items) {
  return value ** 2;
}

const result = map(items, callBackFunction);
console.log(result);

// const eachValue = require("../map.cjs");
// const items = [1, 2, 3, 4, 5, 5];
// const output_map_array = [];

// function map(items, eachValue) {
//   for (let index = 0; index < items.length; index++) {
//     let result = eachValue(items[index], index, items);
//     output_map_array.push(result);
//   }
//   return output_map_array;
// }

// const output = map(items, eachValue);
// console.log(output);
