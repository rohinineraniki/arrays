const eachValue = require("../each.cjs");
const items = [1, 2, 3, 4, 5, 5];

function each(items) {
  for (let i = 0; i < items.length; i++) {
    items[i] = eachValue(items[i], i);
  }
  return items;
}

each(items);
console.log(items);
