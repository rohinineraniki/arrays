const flatten = require("../flatten.cjs");

const nestedArray = [0, 1, 2, [[3, 4]]];

const output = flatten(nestedArray);
console.log(output);
