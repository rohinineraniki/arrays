const reduce = require("../reduce.cjs");
const items = [1, 2, 3, 4, 5, 5];
let accumulator;

function callBackFunction(accumulator, value, index, items) {
  return accumulator + value;
}

const result = reduce(items, callBackFunction, accumulator);
console.log(result);
